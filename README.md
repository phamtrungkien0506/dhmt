# Đồ hoạ máy tính

#### Chủ đề: <Thay đổi trang phục nhân vật>

|STT|                Property                |   Folder dir  |
|:-:|----------------------------------------|---------------|
| 1 |               Unity Asset              |   [Asset][1]  |
| 2 |             Built Project              |   [build][2]  |
| 3 |  Exported Package (Animation Version)  |   [anim][3]   |
| 4 |   Exported Package (T-Pose Version)    |   [tpose][4]  |

WebGL: https://kien-pt.github.io/DHMT/

[1]: https://gitlab.com/phamtrungkien0506/dhmt/-/tree/master/Assets
[2]: https://gitlab.com/phamtrungkien0506/dhmt/-/tree/master/build
[3]: https://gitlab.com/phamtrungkien0506/dhmt/-/blob/master/package/clothes-changing-package.unitypackage
[4]: https://gitlab.com/phamtrungkien0506/dhmt/-/blob/master/package/thay_doi_quan_ao.unitypackage