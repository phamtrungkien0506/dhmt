﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour {
    float startX, endX;
	float offsetX, previousEndX;
    public float speed = 2;
    float notMoveTimeCounter;

    bool pressAndHold;
    Button selectButton;

    public GameObject maleDefaultClothes;

    public GameObject currentClothes;

    int currentScrollViewIndex = 0;
    public GameObject[] fScrollViewTop, fScrollViewBottom, mScrollViewTop, mScrollViewBottom;

    public GameObject[] allClothes, allEyes, allHair, allSkin;
    // public Button currentClothesColorButton, currentHairColorButton, currentSkinColorButton;

    int currentHairIndex = 0, currentGender = 0;

    public GameObject canvas;

    void Start() {

    }

    public void ChangeType(Dropdown dropdown) {
        fScrollViewTop[currentScrollViewIndex].SetActive(false);
        fScrollViewBottom[currentScrollViewIndex].SetActive(false);
        mScrollViewTop[currentScrollViewIndex].SetActive(false);
        mScrollViewBottom[currentScrollViewIndex].SetActive(false);
        currentScrollViewIndex = dropdown.value;
        fScrollViewTop[currentScrollViewIndex].SetActive(true);
        fScrollViewBottom[currentScrollViewIndex].SetActive(true);
        mScrollViewTop[currentScrollViewIndex].SetActive(true);
        mScrollViewBottom[currentScrollViewIndex].SetActive(true);
    }

    public void ChangeGender(Dropdown dropdown) {
        gameObject.transform.GetChild(currentGender).gameObject.SetActive(false);
        canvas.transform.GetChild(currentGender + 3).gameObject.SetActive(false);
        currentGender = dropdown.value;
        gameObject.transform.GetChild(currentGender).gameObject.SetActive(true);
        canvas.transform.GetChild(currentGender + 3).gameObject.SetActive(true);
        GameObject temp = maleDefaultClothes;
        maleDefaultClothes = currentClothes;
        currentClothes = temp;
    }

    public void ChangeEyes(Material material) {
        // currentClothesColorButton.transform.GetChild(0).gameObject.SetActive(false);
        // currentClothesColorButton = button;
        // currentClothesColorButton.transform.GetChild(0).gameObject.SetActive(true);
        
        foreach (GameObject eye in allEyes) {
            eye.GetComponent<SkinnedMeshRenderer>().material = material;
        }
    }

    public void ChangeHair(int index) {
        // currentClothesColorButton.transform.GetChild(0).gameObject.SetActive(false);
        // currentClothesColorButton = button;
        // currentClothesColorButton.transform.GetChild(0).gameObject.SetActive(true);
        
        foreach (GameObject hairList in allHair) {
            hairList.transform.GetChild(currentHairIndex % hairList.transform.childCount).gameObject.SetActive(false);
        }

        currentHairIndex = index;

        foreach (GameObject hairList in allHair) {
            hairList.transform.GetChild(currentHairIndex % hairList.transform.childCount).gameObject.SetActive(true);
        }
    }

    public void ChangeClothes(GameObject clothes) {
        currentClothes.SetActive(false);
        currentClothes = clothes;
        currentClothes.SetActive(true);
    }

    public void ChangeClothesColor() {
        Button button = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.GetComponent<Button>();

        // currentClothesColorButton.transform.GetChild(0).gameObject.SetActive(false);
        // currentClothesColorButton = button;
        // currentClothesColorButton.transform.GetChild(0).gameObject.SetActive(true);
        
        foreach (GameObject clothes in allClothes) {
            int[] part = clothes.GetComponent<MaterialManager>().part;
            for (int i = 0; i < part.Length; i++) {
                int j = part[i];
                clothes.GetComponent<SkinnedMeshRenderer>().materials[j].color = button.GetComponent<Image>().color;
            }
        }
    }

    public void ChangeSkinColor() {
        Button button = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
        
        // currentSkinColorButton.transform.GetChild(0).gameObject.SetActive(false);
        // currentSkinColorButton = button;
        // currentSkinColorButton.transform.GetChild(0).gameObject.SetActive(true);
        
        foreach (GameObject skin in allSkin) {
            skin.GetComponent<SkinnedMeshRenderer>().material.color = button.GetComponent<Image>().color;
        }
    }

    public void ChangeHairColor() {
        Button button = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
        
        // currentHairColorButton.transform.GetChild(0).gameObject.SetActive(false);
        // currentHairColorButton = button;
        // currentHairColorButton.transform.GetChild(0).gameObject.SetActive(true);
        
        foreach (GameObject hairList in allHair) {
            for (int id = 0; id < hairList.transform.childCount; id++) {
                GameObject hair = hairList.transform.GetChild(id).gameObject;
                int[] part = hair.GetComponent<MaterialManager>().part;
                for (int i = 0; i < part.Length; i++) {
                    int j = part[i];
                    hair.GetComponent<SkinnedMeshRenderer>().materials[j].color = button.GetComponent<Image>().color;
                }
            }
        }
    }

    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            pressAndHold = true;
            startX = endX = Input.mousePosition.x;
        }

        if (Input.GetMouseButton(0)) {
            previousEndX = endX;
            endX = Input.mousePosition.x;
            offsetX = endX - startX;
        }

        if (Input.GetMouseButtonUp(0)) {
            pressAndHold = false;
            startX = endX = previousEndX = 0;
        }

        if (pressAndHold) {

            notMoveTimeCounter -= Time.deltaTime;
            if (notMoveTimeCounter > 0) {
                Vector3 vector = new Vector3(0, -offsetX, 0);
                transform.Rotate(vector * speed * Time.deltaTime, Space.World);
            }

            if (Mathf.Abs(previousEndX - endX) >= 1) notMoveTimeCounter = 0.075f;
        }
    }
}
